/* [Top Rim] */
top_rim_height = 20;
top_rim_thickness = 3;
top_rim_clearance = 0.5;
top_rim_upper_extra_radius = 3;
top_rim_upper_extra_height = 5;

/* [Bottom Platform] */
bottom_platform_height = 50;
bottom_platform_clearance = 4;

/* [Precision] */
$fs = $preview ? 2 : 0.5;
$fa = $preview ? 2 : 0.5;
epsilon = 0.1;

module
shape_top()
{
  import("shape-top.svg", center = true);
}

module top_rim(thickness = top_rim_thickness,
               height = top_rim_height,
               offset = 0,
               clearance = top_rim_clearance)
{
  difference()
  {
    linear_extrude(height) offset(offset - clearance) shape_top();
    translate([ 0, 0, -epsilon / 2 ]) linear_extrude(height + epsilon)
      offset(offset - thickness - clearance) shape_top();
  }
}

module
shape_bottom()
{
  import("shape-bottom.svg", center = true);
}

// the top rim
union()
{
  // the base top rim to fix the paper bag
  top_rim();

  // the upper funnel
  translate([ 0, 0, top_rim_height ])
  {
    difference()
    {
      hull()
      {
        translate([ 0, 0, 0 ]) top_rim(height = epsilon);
        translate([ 0, 0, top_rim_upper_extra_height ])
          top_rim(offset = top_rim_upper_extra_radius, height = epsilon);
      }
      hull()
      {
        translate([ 0, 0, -epsilon / 2 ])
          top_rim(offset = -top_rim_thickness, height = epsilon);
        translate([ 0, 0, top_rim_upper_extra_height + epsilon / 2 ])
          top_rim(offset = top_rim_upper_extra_radius - top_rim_thickness,
                  height = epsilon);
      }
    }
  }
}

linear_extrude(bottom_platform_height) offset(-bottom_platform_clearance)
  shape_bottom();
